from copy import copy
from datetime import datetime

from insights4ci.tasks.celery import app
from insights4ci.datasources.gitlab import Project as GLProject
from insights4ci.models import Pipeline, Runner, Job, Test, TestResult
from insights4ci.database import SessionLocal


@app.task
def populate_from_external_data(project_id, external_id):
    db = SessionLocal()
    for pipeline in GLProject(external_id).pipelines:
        db_pipeline = Pipeline.create_from_dict(db,
                                                pipeline.as_dict(),
                                                project_id)
        for job in pipeline.jobs:
            # Let's skip the runner for now
            # if job.runner:
            #     runner_db = Runner.get_by_name_or_create(db, job.name, project_id)
            #     runner_db.description = job.runner.description
            #     runner_db.external_id = job.runner.external_id
            #     started_at = job.started_at[:19]
            #     runner_db.last_seen = datetime.strptime(started_at, '%Y-%m-%dT%H:%M:%S').strftime('%Y-%m-%d %H:%M:%S')
            #     runner_db.save_to_session(db)

            job_db = Job.create_from_dict(db, job.as_dict(), db_pipeline.id)
            for test in job.test_results:
                print(test.name, test.class_name)
                test_db = Test.get_by_name_or_create(db,
                                                     test.name,
                                                     test.class_name,
                                                     project_id)
                TestResult.create_from_dict(db,
                                            test.as_dict(),
                                            test_db.id,
                                            job_db.id)
