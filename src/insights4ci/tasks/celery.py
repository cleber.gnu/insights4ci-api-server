import os

from celery import Celery


# app = Celery(name='insights4ci',
#              broker=os.environ.get("CELERY_BROKER_URL",
#                                    "redis://localhost:6379"),
#              backend=os.environ.get("CELERY_RESULT_BACKEND",
#                                     "redis://localhost:6379"),
#              include=['insights4ci.tasks.projects'])

app = Celery(name='insights4ci',
             broker=os.environ.get("CELERY_BROKER_URL",
                                   "redis://localhost:6379"),
             include=['insights4ci.tasks.gitlab'])


app.conf.update(
    result_expires=3600,
)

if __name__ == '__main__':
    app.start()
